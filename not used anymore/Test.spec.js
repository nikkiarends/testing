import { mount } from "vue-test-utils";
import expect from "expect";
import Countdown from "../src/components/Countdown.vue";
import moment from "moment";
import sinon from "sinon";

describe("Countdown", () => {
  let wrapper, clock;

  beforeEach(() => {
    wrapper = mount(Countdown);
    clock = sinon.useFakeTimers();
  });

  afterEach(() => clock.restore);

  it("renders a countdown timer", () => {
    wrapper.setProps({
      until: moment().add(10, "Seconds")
    });

    see("0 Days");
    see("0 Hours");
    see("0 Minutes");
    see("0 Seconds");
  });

  it("reduces the countodwn every second", () => {
    wrapper.setProps({
      until: moment().add(10, "Seconds")
    });

    see("0 Seconds");

    clock.tick(1000);
    console.log("hello");
    // wrapper.vm.$nextTick(() => {
    //     see('9 Seconds');
    // })
  });

  //Helper functions
  let see = (text, selector) => {
    let wrap = selector ? wrapper.find(selector) : wrapper;
    expect(wrap.html()).toContain(text);
  };
  let type = (selector, text) => {
    let node = wrapper.find(selector);
    node.element.value = text;
    node.trigger("input");
  };
  let click = selector => {
    wrapper.find(selector).trigger("click");
  };
});
